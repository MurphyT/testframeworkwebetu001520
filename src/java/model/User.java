/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Url;
import controller.ModelView;
import java.sql.Date;

/**
 *
 * @author Murphy
 */
public class User {
    
    Integer id;
    String nom;
    String prenom;
    Date datedenaissance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDatedenaissance() {
        return datedenaissance;
    }

    public void setDatedenaissance(Date datedenaissance) {
        this.datedenaissance = datedenaissance;
    }
    
    @Url(name = "liste")
    public ModelView liste () {
        ModelView mv = new ModelView();
        mv.setUtl("liste.jsp");
        mv.addData("id",1);
        mv.addData("nom","jean");
        mv.addData("prenom","rakoto");
        mv.addData("date de naissance",java.sql.Date.valueOf("2022-01-01"));
        return mv;
    }
    
    @Url(name = "save")
    public ModelView save () {
        ModelView mv = new ModelView();
        mv.setUtl("liste.jsp");
        mv.addData("id",id);
        mv.addData("nom",nom);
        mv.addData("prenom",prenom);
        mv.addData("date de naissance",datedenaissance);
        return mv;
    }
    
    
}
