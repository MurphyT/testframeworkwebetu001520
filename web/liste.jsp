<%-- 
    Document   : liste
    Created on : 14 nov. 2022, 15:16:40
    Author     : Murphy
--%>

<%@page import="java.sql.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String nom = (String)request.getAttribute("nom");
    String prenom = (String)request.getAttribute("prenom");
    Date datedenaissance = (Date)request.getAttribute("date de naissance");
    Integer id = (Integer)request.getAttribute("id");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>Id : <%= id %> </p>
        <p>Nom : <%= nom %> </p>
        <p>Prenom : <%= prenom %> </p>
        <p>Date de naissance : <%= datedenaissance %> </p>
    </body>
</html>
